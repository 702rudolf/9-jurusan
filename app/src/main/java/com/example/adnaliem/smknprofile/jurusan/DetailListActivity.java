package com.example.adnaliem.smknprofile.jurusan;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.example.adnaliem.smknprofile.jurusan.dao.DaoSession;
import com.example.adnaliem.smknprofile.jurusan.dao.Profile;
import com.example.adnaliem.smknprofile.jurusan.dao.ProfileDao;
import com.example.adnaliem.smknprofile.jurusan.fragment.DeskripsiFragment;
import com.example.adnaliem.smknprofile.jurusan.fragment.GuruFragment;
import com.example.adnaliem.smknprofile.jurusan.utils.DaoHandler;


/**
 * Created by yuda on 04/02/2016.
 */
public class DetailListActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView ivHeader;

    DaoSession daoSession;
    Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_list);

        toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        String id = getIntent().getStringExtra("id");

        daoSession = DaoHandler.getInstance(this);
        profile = new Profile();
        profile = daoSession.getProfileDao().queryBuilder().where(ProfileDao.Properties.Id.eq(id)).unique();

        collapsingToolbar.setTitle(profile.getName());

        ivHeader = (ImageView) findViewById(R.id.header);
        ivHeader.setImageResource(profile.getMThumbnail());
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());


        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        int NUM_ITEMS = 2;
        Bundle bundle;

        String[] name = {"Deskripsi", "Guru"};

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return DeskripsiFragment.newInstance(profile.getId());
                case 1: // Fragment # 0 - This will show FirstFragment different title
                    return GuruFragment.newInstance(profile.getName());
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return name[position];
        }
    }

}
