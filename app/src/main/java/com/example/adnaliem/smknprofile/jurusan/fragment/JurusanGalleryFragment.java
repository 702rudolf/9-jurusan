package com.example.adnaliem.smknprofile.jurusan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.adnaliem.smknprofile.jurusan.R;
import com.example.adnaliem.smknprofile.jurusan.adapter.CardAdapter;
import com.example.adnaliem.smknprofile.jurusan.adapter.JurusanGalleryAdapter;

/**
 * Created by iksandecade on 12/02/2016.
 */
public class JurusanGalleryFragment extends Fragment {
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;

    public static JurusanGalleryFragment newInstance(String data) {
        JurusanGalleryFragment jurusanGalleryFragment = new JurusanGalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id", data);
        jurusanGalleryFragment.setArguments(bundle);
        return jurusanGalleryFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_jurusan_gallery, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new JurusanGalleryAdapter(getActivity());
        mRecyclerView.setAdapter(mAdapter);
        return rootView;
    }
}
