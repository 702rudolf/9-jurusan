package com.example.adnaliem.smknprofile.jurusan.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adnaliem.smknprofile.jurusan.ListGuruActivity;
import com.example.adnaliem.smknprofile.jurusan.R;
import com.example.adnaliem.smknprofile.jurusan.dao.DaoSession;
import com.example.adnaliem.smknprofile.jurusan.dao.Profile;
import com.example.adnaliem.smknprofile.jurusan.dao.ProfileDao;
import com.example.adnaliem.smknprofile.jurusan.utils.DaoHandler;


public class DeskripsiFragment extends Fragment {
    public TextView tvDesNature;
    public TextView tvMDesc;
    String s;
    DaoSession daoSession;
    Profile profile;

    public static DeskripsiFragment newInstance(String data) {
        DeskripsiFragment deskripsiFragment = new DeskripsiFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id", data);
        deskripsiFragment.setArguments(bundle);
        return deskripsiFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        s = getArguments().getString("id");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_deskripsi, container, false);

        daoSession = DaoHandler.getInstance(getActivity());

        tvDesNature = (TextView) rootView.findViewById(R.id.tv_des_nature);
        tvMDesc = (TextView) rootView.findViewById(R.id.tvMDesc);

        profile = new Profile();
        profile = daoSession.getProfileDao().queryBuilder().where(ProfileDao.Properties.Id.eq(s)).unique();
        tvDesNature.setText(profile.getMDes());
        tvMDesc.setText(profile.getMDesc());
        return rootView;
    }
}
