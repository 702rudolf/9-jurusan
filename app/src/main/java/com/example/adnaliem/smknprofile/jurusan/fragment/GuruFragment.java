package com.example.adnaliem.smknprofile.jurusan.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.adnaliem.smknprofile.jurusan.ListGuruActivity;
import com.example.adnaliem.smknprofile.jurusan.R;
import com.example.adnaliem.smknprofile.jurusan.adapter.GuruAdapter;

/**
 * Created by iksandecade on 04/02/2016.
 */
public class GuruFragment extends Fragment {

    String s;

    public static GuruFragment newInstance(String data) {
        GuruFragment guruFragment = new GuruFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id", data);
        guruFragment.setArguments(bundle);
        return guruFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        s = getArguments().getString("id");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_guru, container, false);
        CardView cv = (CardView) rootView.findViewById(R.id.cvGuru);
        cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ListGuruActivity.class);
                i.putExtra("jurusan", s);
                startActivity(i);
            }
        });
        return rootView;
    }
}
