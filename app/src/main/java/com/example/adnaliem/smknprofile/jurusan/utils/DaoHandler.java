package com.example.adnaliem.smknprofile.jurusan.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.adnaliem.smknprofile.jurusan.dao.DaoMaster;
import com.example.adnaliem.smknprofile.jurusan.dao.DaoSession;

/**
 * Created by yuda on 09/01/2016.
 */
public class DaoHandler {
    public static DaoSession getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "lovers-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();

        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession mDaoSession = daoMaster.newSession();
        return mDaoSession;
    }
}
