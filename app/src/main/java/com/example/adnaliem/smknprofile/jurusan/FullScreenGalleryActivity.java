package com.example.adnaliem.smknprofile.jurusan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.adnaliem.smknprofile.jurusan.adapter.GalleryAdapter;

public class FullScreenGalleryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_gallery);
        Intent i = getIntent();

        // Selected image id
        int position = i.getExtras().getInt("id");
        GalleryAdapter imageAdapter = new GalleryAdapter(this);

        ImageView imageView = (ImageView) findViewById(R.id.full_image_view);
        imageView.setImageResource(imageAdapter.gambar[position]);

    }
}
