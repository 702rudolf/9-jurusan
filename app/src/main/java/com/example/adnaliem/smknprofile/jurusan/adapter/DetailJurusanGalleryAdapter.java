package com.example.adnaliem.smknprofile.jurusan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.adnaliem.smknprofile.jurusan.R;

/**
 * Created by iksandecade on 12/02/2016.
 */
public class DetailJurusanGalleryAdapter extends BaseAdapter {

    LayoutInflater layoutInflater;
    public Integer[] gambar1 = {
            R.drawable.gambar1,
            R.drawable.gambar2
    };
    public Integer[] gambar2 = {
            R.drawable.gambar2,
            R.drawable.gambar3
    };
    public Integer[] gambar3 = {
            R.drawable.gambar3,
            R.drawable.gambar4
    };

    public Integer[] gambar = {
            R.drawable.gambar1,
            R.drawable.gambar2,
            R.drawable.gambar3,
            R.drawable.gambar4
    };

    public Integer[] full;

    Context context;

    String jurusan;

    public DetailJurusanGalleryAdapter(Context context, String jurusan) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        if(jurusan.equals("TPTU")){
            full = gambar1;
        } else if (jurusan.equals("TOI")) {
            full = gambar2;
        } else if (jurusan.equals("TPPP")){
            full = gambar3;
        } else{
            full = gambar;
        }
    }

    @Override
    public int getCount() {


        return full.length;
    }

    @Override
    public Object getItem(int position) {
        return full[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View root = layoutInflater.inflate(R.layout.item_gallery_gridview, parent, false);
        ImageView ivGallery = (ImageView) root.findViewById(R.id.ivGallery);
        ivGallery.setImageResource(full[position]);
        return root;
    }
}
