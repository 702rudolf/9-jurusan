package com.example.adnaliem.smknprofile.jurusan.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.adnaliem.smknprofile.jurusan.JurusanGalleryActivity;
import com.example.adnaliem.smknprofile.jurusan.R;
import com.example.adnaliem.smknprofile.jurusan.dao.DaoSession;
import com.example.adnaliem.smknprofile.jurusan.dao.Profile;
import com.example.adnaliem.smknprofile.jurusan.utils.DaoHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by iksandecade on 12/02/2016.
 */
public class JurusanGalleryAdapter extends RecyclerView.Adapter<JurusanGalleryAdapter.ViewHolder> {
    List<Profile> mItems;
    Activity activity;

    DaoSession daoSession;

    public JurusanGalleryAdapter(Activity activity) {
        super();

        this.activity = activity;
        daoSession = DaoHandler.getInstance(activity);
        mItems = new ArrayList<Profile>();
        mItems = daoSession.getProfileDao().queryBuilder().list();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycle_view_card_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final Profile profile = mItems.get(i);
        viewHolder.tvName.setText(profile.getName());
        viewHolder.tvSubname.setText(profile.getSubName());
        viewHolder.ivLogo.setImageResource(profile.getMLogo());
        viewHolder.topLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, JurusanGalleryActivity.class);
                i.putExtra("jurusan", profile.getName());
                activity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        public TextView tvName;
        public TextView tvSubname;
        public ImageView ivLogo;
        public RelativeLayout topLayout;//ini untuk trigger aksi intent, dijadikan seperti button
        public CardView cvJurusan;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvSubname = (TextView) itemView.findViewById(R.id.tvSubName);
            topLayout = (RelativeLayout) itemView.findViewById(R.id.top_layout);
            ivLogo = (ImageView) itemView.findViewById(R.id.ivLogo);
        }
    }
}
