package com.example.adnaliem.smknprofile.jurusan.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.adnaliem.smknprofile.jurusan.R;
import com.example.adnaliem.smknprofile.jurusan.dao.DaoSession;
import com.example.adnaliem.smknprofile.jurusan.dao.Guru;
import com.example.adnaliem.smknprofile.jurusan.dao.GuruDao;
import com.example.adnaliem.smknprofile.jurusan.utils.DaoHandler;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by adnaliem on 09/01/2016.
 */
public class GuruAdapter extends RecyclerView.Adapter<GuruAdapter.ViewHolder> {
    List<Guru> mItems;
    Activity activity;

    DaoSession daoSession;

    public GuruAdapter(Activity activity, String jurusan) {
        super();

        this.activity = activity;
        daoSession = DaoHandler.getInstance(activity);
        mItems = new ArrayList<Guru>();
        mItems = daoSession.getGuruDao().queryBuilder().where(GuruDao.Properties.Jurusan.eq(jurusan)).list();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycle_guru_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Guru profile = mItems.get(position);
        holder.tvSubname.setText(profile.getNama());
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        public TextView tvSubname;

        public ViewHolder(View itemView) {
            super(itemView);

            tvSubname = (TextView) itemView.findViewById(R.id.tvGuru);

        }









    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

}
