package com.example.adnaliem.smknprofile.jurusan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.adnaliem.smknprofile.jurusan.R;

/**
 * Created by iksandecade on 12/02/2016.
 */
public class GalleryAdapter extends BaseAdapter {

    LayoutInflater layoutInflater;
    public Integer[] gambar = {
            R.drawable.gambar1,
            R.drawable.gambar2,
            R.drawable.gambar3,
            R.drawable.gambar4
    };

    Context context;

    public GalleryAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return gambar.length;
    }

    @Override
    public Object getItem(int position) {
        return gambar[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View root = layoutInflater.inflate(R.layout.item_gallery_gridview, parent, false);
        ImageView ivGallery = (ImageView) root.findViewById(R.id.ivGallery);
        ivGallery.setImageResource(gambar[position]);
        return root;
    }
}
