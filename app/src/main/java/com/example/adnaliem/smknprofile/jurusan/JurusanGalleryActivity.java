package com.example.adnaliem.smknprofile.jurusan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.adnaliem.smknprofile.jurusan.adapter.DetailJurusanGalleryAdapter;
import com.example.adnaliem.smknprofile.jurusan.adapter.GalleryAdapter;

public class JurusanGalleryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jurusan_gallery);
        GridView gridView = (GridView) findViewById(R.id.gridView);

        // Instance of ImageAdapter Class
        gridView.setAdapter(new DetailJurusanGalleryAdapter(this, getIntent().getStringExtra("jurusan")));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                // Sending image id to FullScreenActivity
                Intent i = new Intent(JurusanGalleryActivity.this, FullScreenGalleryActivity.class);
                // passing array index
                i.putExtra("id", position);
                startActivity(i);
            }
        });
    }
}
