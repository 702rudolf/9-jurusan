package com.example.adnaliem.smknprofile.jurusan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adnaliem.smknprofile.jurusan.adapter.CardAdapter;
import com.example.adnaliem.smknprofile.jurusan.dao.DaoSession;
import com.example.adnaliem.smknprofile.jurusan.dao.Guru;
import com.example.adnaliem.smknprofile.jurusan.dao.Profile;
import com.example.adnaliem.smknprofile.jurusan.utils.DaoHandler;


import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "OVK9NHo0mC0M4HGe4JUMslb5p";
    private static final String TWITTER_SECRET = "Zkvvp7HUtAm4FSDLhQeeXBsMCpo2atBwaQVtevxbm1pitIWW53";

    private TwitterLoginButton loginButton;
    TextView textView;
    TwitterSession session;


    ImageView ivExplore;

    DaoSession daoSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_main);

        ivExplore = (ImageView) findViewById(R.id.ivExplore);

        ivExplore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListJurusanActivity.class);
                startActivity(i);
            }
        });

        daoSession = DaoHandler.getInstance(this);
        addProfile();
        addGuru();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {
            Intent i = new Intent(this, MapsActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_about) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_galery) {
            Intent i = new Intent(this, GaleryActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void addGuru() {
        Guru guru = new Guru();
        guru.setId("002");
        guru.setNama("agus nugroho");
        guru.setJurusan("TPTU");


        daoSession.getGuruDao().insertOrReplace(guru);

        guru = new Guru();
        guru.setId("003");
        guru.setNama("yuli pamungkas");
        guru.setJurusan("TPTU");
        daoSession.getGuruDao().insertOrReplace(guru);

        guru = new Guru();
        guru.setId("004");
        guru.setNama("abraham");
        guru.setJurusan("TPTU");
        daoSession.getGuruDao().insertOrReplace(guru);

        guru = new Guru();
        guru.setId("005");
        guru.setNama("dani");
        guru.setJurusan("TPTU");
        daoSession.getGuruDao().insertOrReplace(guru);

        guru = new Guru();
        guru.setId("006");
        guru.setNama("ujang");
        guru.setJurusan("TPTU");
        daoSession.getGuruDao().insertOrReplace(guru);

        guru = new Guru();
        guru.setId("007");
        guru.setNama("wira");
        guru.setJurusan("TPTU");
        daoSession.getGuruDao().insertOrReplace(guru);
    }

    public void addProfile() {
        Profile profile = new Profile();
        profile.setId("1");
        profile.setName("TPTU");
        profile.setSubName("Teknik Pendingin dan tata udara");
        profile.setMDes("\n" +
                "Teknik Pendingin/Refrigerasi dan Tata Udara adalah dua disiplin ilmu yang saling terkait serta berhubungan dengan sejumlah disiplin ilmu yang lainnya, seperti Teknik Mesin, Teknik Listrik, Teknik Sipil & Arsitektur, Teknologi Makanan & Kesehatan, Teknik Fisika, Teknik Kimia. Sistem Refrigerasi tidak hanya digunakan di rumah tangga seperti kulkas, namun juga pada kegiatan komersial seperti di pasar swalayan, restoran, gudang, hotel, untuk pengawetan/pendinginan makanan, pendinginan minuman dll. Disamping itu juga untuk kegiatan transportasi darat-laut dan udara, serta digunakan di industri-industri, misalnya: Industri Kimia Industri Susu Pabrik Es Industri Gas Industri Agro (Pertanian, Perkebunan, Peternakan) Industri Perikanan/Kelautan dan lain-lain Sedangkan Sistem Tata Udara tidak hanya mencakup bidang kenyamanan saja, baik di rumah tangga seperti AC split dan AC window, tapi juga digunakan di apartemen, perkantoran, perbankan, rumah makan/cafe, perhotelan, gedung pertemuan, konvensi, mall, airport, rumah-rumah sakit untuk ruang operasi, dll. Disamping itu, sistem ini digunakan di industri-industri, misalnya untuk Proses Kimia Steril, Clean Room, Produksi Microchip, Gudang Penyimpanan, serta untuk Transportasi Darat-Laut dan Udara. Karena sedemikian luasnya kebutuhan akan sistem Refrigerasi dan Tata Udara, untuk itu perlu dididik calon tenaga-tenaga ahli yang Terampil, Profesional, Kompeten dan Menguasai Bidang Teknik Refrigerasi dan Tata Udara.");
        profile.setMThumbnail(R.drawable.tp);
        profile.setMLogo(R.drawable.ic_tp);
        profile.setMDesc("KOMPETENSI KEAHLIAN TEKNIK PENDINGIN :\n" +
                "\n" +
                "1. Melaksanakan Pekerjaan Secara Aman\n" +
                "2. Mengukur Besaran Listrik & Udara\n" +
                "3. Memeriksa Fungsi & Performance Peralatan\n" +
                "4. Mengisi Refrigerant Mesin Pendingin\n" +
                "5. Menggambar Sistem Instalasi Pemipaan Refrigerant\n" +
                "6. Sistem Bilangan & sistem Logika Gerbang\n" +
                "7. Mengoperasikan & Melayani Cold Storage, Pabrik Es Komersil\n" +
                "8. Merawat & Memperbaiki Ventilasi Udara\n" +
                "9. Merawat & Memperbaiki Kompresor Mesin Pendingin\n" +
                "10. Membaca Gambar Sistem Pemipaan Refrigerasi\n" +
                "11. Menggambar Sistem Ducting Teknik Pendingin\n" +
                "12. Mengontrol Sistem Kelistrikan, Mekanik Refrigerasi\n" +
                "13. Pemograman Sistem Refrigerasi Secara Elektronik\n" +
                "14. Merawat dan Memperbaiki Alat Penukar Kalor\n" +
                "15. Merawat dan Memperbaiki Mesin Pendingin\n" +
                "16. Pengoprasian & Melayani Cold Storage\n" +
                "17. Penerapan Mikro Kontoler Pada Sistem Refrigerasi\n");
        daoSession.getProfileDao().insertOrReplace(profile);

        profile = new Profile();
        profile.setId("2");
        profile.setName("TOI");
        profile.setSubName("Teknik Otomasi Industri");
        profile.setMDes("Listrik Industri adalah salah satu jurusan yang ada di SMKN 1 Cimahi. Jurusan ini bergerak di bidang Proteksi Sistem Tenaga Listrik, Pengendali Elektro Hidrolik dan Elektro Pneumatik, Pengendali Elektronik dan PLC, dsb.");
        profile.setMThumbnail(R.drawable.toi);
        profile.setMLogo(R.drawable.ic_toi);
        profile.setMDesc("KOMPETENSI KEAHLIAN LISTRIK INDUSTRI :\n" +
                "\n" +
                "1. Menguasai Gambar Teknik Listrik & Elektronika\n" +
                "2. Menguasai Konsep Dasar Listrik dan Elektronika\n" +
                "3. Menguasai Alat ukur listrik dan elektronika\n" +
                "4. Menguasai Pekerjaan Mekanik Listrik dan Elektronika\n" +
                "5. Mengerjakan dan memperbaiki Peralatan Listrik Rumah Tangga\n" +
                "6. Memasang Dasar Instalasi Listrik\n" +
                "7. Merencanakan dan Memasang Instalasi Listrik Penerangan dan Tenaga\n" +
                "8. Menguji Karakteristik Mesin Listrik DC\n" +
                "9. Merawat dan Memperbaiki Mesin Listrik DC\n" +
                "10. Merangkai Rangkaian Pengendali Dasar Mekanik dan Magnetik\n" +
                "11. Merangkai Rangkaian Pengendali Dasar Elektronik\n" +
                "12. Menguji Karakteristik Generator AC\n" +
                "13. Merawat dan Memperbaiki Generator AC\n" +
                "14. Menguji Karakteristik Motor 1 fasa dan Motor 3 fasa\n" +
                "15. Merawat dan Memperbaiki Motor AC 3 Fasa\n" +
                "16. Memasang dan Mengoperasikan Pengendali Elektronik\n" +
                "17. Memasang dan Mengoperasikan Sistem Pengendali Elektromagnetik\n" +
                "18. Memasang dan Mengoperasikan Sistem Pengendali Pneumatik\n" +
                "19. Memasang dan Mengoperasikan Sistem Pengendali Elektronika Daya\n" +
                "20. Menguji Karakteristik Transformator 1 Fasa dan 3 Fasa\n" +
                "21. Merawat dan Memperbaiki Motor AC 1 Fasa");
        daoSession.getProfileDao().insertOrReplace(profile);

        profile = new Profile();
        profile.setId("3");
        profile.setName("TPPPP");
        profile.setSubName("Teknik Produksi & Penyiaran Program Pertelevisian");
        profile.setMDes("Teknik Produksi dan Penyiaran Program Pertelevisian atau yang sering disingkat menjadi \"TP4\" adalah salah satu jurusan yang ada di SMKN 1 Cimahi yang bergerak di bidang produksi dan broadcasting program pertelevisian.\n");
        profile.setMThumbnail(R.drawable.tp4);
        profile.setMLogo(R.drawable.ic_tp4);
        profile.setMDesc("KOMPETENSI KEAHLIAN TEKNIK PRODUKSI PENYIARAN PROGRAM PRETELEVISIAN :\n" +
                "\n" +
                "1. Prosedur kesehatan, keselamatan dan keamanan kerja\n" +
                "2. Teknik Fotografi\n" +
                "3. Mengoperasikan Kamera Foto\n" +
                "4. Teknik Penulisan Naskah\n" +
                "5. Membuat naskah drama, non drama dan news\n" +
                "6. Teknik Penyutradaraan\n" +
                "7. Mengarahkan kru dan pemain\n" +
                "8. Menterjemahkan naskah dalam bentuk audio visual\n" +
                "9. Manajemen Produksi\n" +
                "10. Membuat dan menyusun perencanaan sebuah produksi\n" +
                "11. Melakukan survey lokasi\n" +
                "12. Tata Artistik\n" +
                "13. Membuat gambar1-gambar1 perencanaan\n" +
                "14. Merancang set untuk keperluan syuting\n" +
                "15. Membuat konstruksi set\n" +
                "16. Videografi\n" +
                "17. Menyiapkan kamera\n" +
                "18. Mengatur fokus\n" +
                "19. Melakukan instalasi dan monitoring peralatan tata lampu\n" +
                "20. Tata Suara\n" +
                "21. Memasang, mengoperasikan perangkat suara\n" +
                "22. Melakukan proses dubbing\n" +
                "23. Membuat dan memilih materi suara untuk kepentingan produksi\n" +
                "24. Teknik Editing\n" +
                "25. Memproduksi program acara televisi\n");
        daoSession.getProfileDao().insertOrReplace(profile);

        profile = new Profile();
        profile.setId("4");
        profile.setName("KM");
        profile.setSubName("Kontrol Mekanik");
        profile.setMDes("Kontrol Mekanik adalah salah satu jurusan yang ada di SMKN 1 Cimahi. Jurusan ini bergerak di bidang Gambar dan perencanaan sistem Kontrol Mekanik , Pengukuran dan pengontrolan besaran proses , Pemasangan instalasi dan pemipaan system kontrol Mekanik dan Pengoperasian, pemeliharaan, dan perbaikan peralatan serta sistem kontrol mekanik. \n");
        profile.setMThumbnail(R.drawable.km);
        profile.setMLogo(R.drawable.ic_km);
        profile.setMDesc("KOMPETENSI KEAHLIAN KONTROL MEKANIK :\n" +
                "\n" +
                "1. Menerapkan prinsip gambar1 komponen kontrol Mekanik\n" +
                "2. Menggunakan alat-alat tangan untuk pengerjaan listrik dan mekanik pada pemipaan\n" +
                "3. Menerapkan prinsip hukum-hukum kelistrikan elektronika dan pneumatik pada system kontrol proses\n" +
                "4. Menerapkan prinsip-prinsip pengukuran besaran kontrol mekanik\n" +
                "5. Menguji, mengkalibrasi, memelihara dan memperbaiki peralatan kontrol mekanik\n" +
                "6. Menerapkan dan menganalisis prinsip hukum kelistrikan elektronika dan pneumatik\n" +
                "7. Mengoperasikan dan memelihara komponen penunjang kontrol mekanik\n" +
                "8. Menerapkan prinsip-prinsip pengontrolan\n" +
                "9. Memasang instalasi dan pemipaan peralatan kontrol mekanik \n" +
                "10. Menggambar dan merencana system kontrol mekanik.");
        daoSession.getProfileDao().insertOrReplace(profile);

        profile = new Profile();
        profile.setId("5");
        profile.setName("KP");
        profile.setSubName("Kontrol Proses");
        profile.setMDes("Kontrol Proses adalah salah satu jurusan yang ada di SMKN 1 Cimahi. Jurusan Kontrol Proses ini hanya ada satu di indonesia yaitu di SMKN 1 Cimahi. Jurusan ini bergerak di bidang Gambar dan Perencanaan system kontrol Proses , Pengukuran dan pengontrolan besaran proses , dan Pengoperasian, pemeliharaan, dan perbaikan peralatan serta system kontrol proses.\n");
        profile.setMThumbnail(R.drawable.kp);
        profile.setMLogo(R.drawable.ic_kp);
        profile.setMDesc("KOMPETENSI KEAHLIAN KONTROL PROSES :\n" +
                "\n" +
                "1. Menguasai konsep dasar kelistrikan dan elektronika\n" +
                "2. Menguasai pekerjaan mekanik dan elektrik\n" +
                "3. Menguasai konsep dasar pengukuran listrik dan elektronika\n" +
                "4. Menguasai gambar1 teknik instrumentasi 1\n" +
                "5. Menguasai dan mengoperasikan power supply pneumatic\n" +
                "6. Menguasai teknik elektronika analog\n" +
                "7. Menguasai teknik elektronika digital\n" +
                "8. Menguasai macam-macam pengukuran besaran proses\n" +
                "9. Menguasai gambar1 teknik instrumentasi 2\n" +
                "10. Menganalisa besaran-besaran proses\n" +
                "11. Mengkalibrasi dan menginstalasi kompoonen kontrol proses\n" +
                "12. Mengaplikasikan mikrokontroler pada sistem kontrol proses\n" +
                "13. Menguasai teknik pengendalian PLC\n" +
                "14. Menguasai gambar1 teknik instrumentasi 3\n" +
                "15. Mengkonstruksi dan menginstalasi serta mengoperasikan sistem kontrol proses\n" +
                "16. Merawat dan memperbaiki komponen sistem kontrol Proses\n" +
                "17. Menggambar dan merencanakan sistem kontrol proses\n");
        daoSession.getProfileDao().insertOrReplace(profile);

        profile = new Profile();
        profile.setId("6");
        profile.setName("TEK");
        profile.setSubName("Teknik Elektronika Komunikasi");
        profile.setMDes("Teknik Transmisi atau yang sering disingkat mejadi \"Tetran\" adalah salah satu jurusan yang ada di SMKN 1 Cimahi. Jurusan ini bergerak dibidang Pengoperasian, Perakitan, Perawatan dan perbaikan peralatan Teknik Transmisi ( Teknik Televisi, Gelombang Micro, Komunikasi Data, dan Komunikasi Radio.\n");
        profile.setMThumbnail(R.drawable.tetran);
        profile.setMLogo(R.drawable.ic_tetran);
        profile.setMDesc("KOMPETENSI KEAHLIAN TEKNIK TRANSMISI :\n" +
                "\n" +
                "1. Komponen elektronika\n" +
                "2. Membaca sifat aplikasai pada rangkaian dc dan ac\n" +
                "3. Perakitan rangkaian elektronika\n" +
                "4. Alat ukur dan digital dasar\n" +
                "5. Dasar sistem komunikasi radio dan pembuatan\n" +
                "6. Teknik digital\n" +
                "7. Pemrograman\n" +
                "8. Pemancar dan antena\n" +
                "9. Teknik televisi\n" +
                "10. Teknik gelombang mikro\n" +
                "11. Komunikasi data\n" +
                "12. Teknik Modulasi\n" +
                "13. Perbaikan peralatan elektronika transmmisi\n" +
                "14. Komunikasi Satelit\n" +
                "15. Handphone\n" +
                "16. Radio pada jaringan komputer\n" +
                "17. Telephone\n");
        daoSession.getProfileDao().insertOrReplace(profile);

        profile = new Profile();
        profile.setId("7");
        profile.setName("TEI");
        profile.setSubName("Teknik Elektronika Industri");
        profile.setMDes("Elektronika Industri dan Komputer adalah salah satu jurusan yang ada di SMKN 1 Cimahi yang bergerak di bidang Teknisi Elektronika Industri.");
        profile.setMThumbnail(R.drawable.eind);
        profile.setMLogo(R.drawable.ic_eind);
        profile.setMDesc("KOMPETENSI KEAHLIAN ELEKTRONIKA INDUSTRI DAN KOMPUTER :\n" +
                "\n" +
                "1. Melaksanakan keselamatan dan kesehatan kerja\n" +
                "2. Mengusai teori dasar elektronika\n" +
                "3. Mengerjakan dasar â€“ dasar pekerjaan bengkel elektronika\n" +
                "4. Mengusai dasar elektronika digital dan komputer\n" +
                "5. Mengukur besaran listrik menggunaka alat ukur analog dan digital\n" +
                "6. Menguasai Elektronika dasar terapan\n" +
                "7. Merakit dan mengoperasikan komputer menggunakan sistem operasi DOS dan Windows.\n" +
                "8. Memrogram peralatan sistem otomasi elektronik yang berkaitan dengan I/O berbantuan: Mikroprosesor dan mikrokontroler\n" +
                "9. Memrogram peralatan sistem otomasi elektronik yang berkaitan dengan I/O berbantuan: PLC,Komputer dan Pneumatic\n" +
                "10. Menggambar teknik elektronika menggunakan komputer\n" +
                "11. Merencanakan pemeliharaaan peralatan elektronik sistem otomasi elektronika\n" +
                "12. Merakit peralatan dan perangkat elektronik sistem otomasi elektronika\n" +
                "13. Merencanakan pemeliharaan peralatan elektronika robot produksi\n" +
                "14. Menginstal peralatan dan perangkat elektronika serta jaringan sistem otomasi â€“ otomasi elektronik\n" +
                "15. Memonitor kinerja operasional sistem otomasi elektronika melalui panel MMI");
        daoSession.getProfileDao().insertOrReplace(profile);

        profile = new Profile();
        profile.setId("8");
        profile.setName("TKJ");
        profile.setSubName("Teknik Komputer dan Jaringan");
        profile.setMDes("Teknik Komputer Jaringan adalah Program Keahlian Baru yang ada di SMK Negeri 1 Cimahi, Program Keahlian ini dibuka karena adanya kemajuan Teknologi dibidang Informasi Teknologi dan Komunikasi yang biasa disebut ICT. Program Keahlian ini dibuka pada Tahun Pelajaran 2003-2004 dan sekarang sudah memiliki Alumni 9 angkatan.\n" +
                "\n" +
                "Pada tahun 2005-2006 Program Keahlian TKJ dicalonkan untuk menjadi Bagian dari Rintisan Sekolah Bertaraf Internasional dan terpilih sebagai Rangking 1 dan setelah diverifikasi dari Jakarta akhirnya pada Tahun Pelajaran 2006-2007 TKJ diberi bantuan sebagai Sekolah Bertaraf Internasional yang waktu itu hanya untuk 1 Program Keahlian.\n" +
                "\n" +
                "Dari hasil Laporan dan Verifikasi Tahun 2007-2008 akhirnya SMK Negeri 1 Cimahi ditetapkan kembali sebagai Rintisan Sekolah Bertaraf Internasional dimana untuk tahun ini tidak Hanya Program Keahlian TKJ saja melainkan untuk 1 sekolah yaitu SMK Negeri 1 Cimahi\n" +
                "\n" +
                "Motto Program keahlian TKJ SMART\n" +
                "Sopan raMah Aktif Rajin Trampil\n" +
                "\n" +
                "Prestasi Yang Sudah diraih\n" +
                "1. Juara I Nasional Lomba Kompetensi Siswa Bidang Teknologi Informasi tahun 2005-2006\n" +
                "2. Juara 2 Tingkat Jawa barat Lomba Kompetensi Siswa Bidang Teknologi Informasi tahun 2006-2007");
        profile.setMThumbnail(R.drawable.tkj);
        profile.setMLogo(R.drawable.ic_tkj);
        profile.setMDesc("Kompetensi Dasar :\n" +
                "1. Komputer Terapan\n" +
                "2. Komunikasi Data\n" +
                "3. Sistem Operasi Jaringan\n" +
                "4. Administrasi Server\n" +
                "5. Rancang Bangun Jaringan\n" +
                "6. Jaringan Nirkabel\n" +
                "7. Keamanan Jaringan\n" +
                "8. Troubleshooting Jaringan\n" +
                "9. Kerja Proyek");
        daoSession.getProfileDao().insertOrReplace(profile);

        profile = new Profile();
        profile.setId("9");
        profile.setName("RPL");
        profile.setSubName("Rekayasa Perangkat Lunak");
        profile.setMDes("Rekayasa Perangkat Lunak adalah salah satu jurusan termuda yang ada di SMKN 1 Cimahi. Jurusan ini bergerak di bidang pembuatan software desktop maupun web serta Pembuatan Game dan Animasi.");
        profile.setMThumbnail(R.drawable.rpl);
        profile.setMLogo(R.drawable.ic_rpl);
        profile.setMDesc("Kompetensi Dasar :\n" +
                "1. Pemograman Dasar\n" +
                "2. Sistem Komputer\n" +
                "3. Perakitan Komputer\n" +
                "4. Simulasi Digital\n" +
                "5. Sistem Operasi\n" +
                "6. Jaringan Dasar\n" +
                "7. Pemograman Web\n" +
                "\n" +
                "Paket Keahlian :\n" +
                "1. Pemodelan Perangkat Lunak\n" +
                "2. Pemrograman Desktop\n" +
                "3. Pemrograman Berorientasi Obyek\n" +
                "4. Basis Data\n" +
                "5. Pemrograman Web Dinamis\n" +
                "6. Pemrograman Grafik\n" +
                "7. Pemrograman Perangkat Bergerak\n" +
                "8. Administrasi Basis Data\n" +
                "9. Kerja Proyek");
        daoSession.getProfileDao().insertOrReplace(profile);
    }

}
