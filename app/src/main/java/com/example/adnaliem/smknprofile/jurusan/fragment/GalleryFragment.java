package com.example.adnaliem.smknprofile.jurusan.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.adnaliem.smknprofile.jurusan.FullScreenGalleryActivity;
import com.example.adnaliem.smknprofile.jurusan.R;
import com.example.adnaliem.smknprofile.jurusan.adapter.GalleryAdapter;

/**
 * Created by iksandecade on 12/02/2016.
 */
public class GalleryFragment extends Fragment {

    public static GalleryFragment newInstance(String data) {
        GalleryFragment galleryFragment = new GalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id", data);
        galleryFragment.setArguments(bundle);
        return galleryFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
        GridView gridView = (GridView) rootView.findViewById(R.id.gridView);

        // Instance of ImageAdapter Class
        gridView.setAdapter(new GalleryAdapter(getActivity()));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                // Sending image id to FullScreenActivity
                Intent i = new Intent(getActivity(), FullScreenGalleryActivity.class);
                // passing array index
                i.putExtra("id", position);
                startActivity(i);
            }
        });
        return rootView;
    }
}
