package com.example.adnaliem.smknprofile.jurusan.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table Profile.
*/
public class ProfileDao extends AbstractDao<Profile, String> {

    public static final String TABLENAME = "Profile";

    /**
     * Properties of entity Profile.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, String.class, "id", true, "ID");
        public final static Property Name = new Property(1, String.class, "name", false, "NAME");
        public final static Property MDes = new Property(2, String.class, "mDes", false, "M_DES");
        public final static Property SubName = new Property(3, String.class, "subName", false, "SUB_NAME");
        public final static Property MDesc = new Property(4, String.class, "mDesc", false, "M_DESC");
        public final static Property MThumbnail = new Property(5, Integer.class, "mThumbnail", false, "M_THUMBNAIL");
        public final static Property MLogo = new Property(6, Integer.class, "mLogo", false, "M_LOGO");
    };


    public ProfileDao(DaoConfig config) {
        super(config);
    }
    
    public ProfileDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'Profile' (" + //
                "'ID' TEXT PRIMARY KEY NOT NULL ," + // 0: id
                "'NAME' TEXT," + // 1: name
                "'M_DES' TEXT," + // 2: mDes
                "'SUB_NAME' TEXT," + // 3: subName
                "'M_DESC' TEXT," + // 4: mDesc
                "'M_THUMBNAIL' INTEGER," + // 5: mThumbnail
                "'M_LOGO' INTEGER);"); // 6: mLogo
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'Profile'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Profile entity) {
        stmt.clearBindings();
 
        String id = entity.getId();
        if (id != null) {
            stmt.bindString(1, id);
        }
 
        String name = entity.getName();
        if (name != null) {
            stmt.bindString(2, name);
        }
 
        String mDes = entity.getMDes();
        if (mDes != null) {
            stmt.bindString(3, mDes);
        }
 
        String subName = entity.getSubName();
        if (subName != null) {
            stmt.bindString(4, subName);
        }
 
        String mDesc = entity.getMDesc();
        if (mDesc != null) {
            stmt.bindString(5, mDesc);
        }
 
        Integer mThumbnail = entity.getMThumbnail();
        if (mThumbnail != null) {
            stmt.bindLong(6, mThumbnail);
        }
 
        Integer mLogo = entity.getMLogo();
        if (mLogo != null) {
            stmt.bindLong(7, mLogo);
        }
    }

    /** @inheritdoc */
    @Override
    public String readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Profile readEntity(Cursor cursor, int offset) {
        Profile entity = new Profile( //
            cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // name
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // mDes
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // subName
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // mDesc
            cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5), // mThumbnail
            cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6) // mLogo
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Profile entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
        entity.setName(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setMDes(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setSubName(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setMDesc(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setMThumbnail(cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5));
        entity.setMLogo(cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6));
     }
    
    /** @inheritdoc */
    @Override
    protected String updateKeyAfterInsert(Profile entity, long rowId) {
        return entity.getId();
    }
    
    /** @inheritdoc */
    @Override
    public String getKey(Profile entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
