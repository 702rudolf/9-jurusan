package pl.surecase.eu;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(9, "com.example.iksandecade.smknprofile.dao");

        addProfile(schema);
        addGuru(schema);


        new DaoGenerator().generateAll(schema, args[0]);
    }


    private static void addProfile(Schema schema) {
        Entity profile = schema.addEntity("Profile");
        profile.setHasKeepSections(true);
        profile.setTableName("Profile");
        profile.addStringProperty("id").primaryKey();
        profile.addStringProperty("name");
        profile.addStringProperty("mDes");
        profile.addStringProperty("subName");
        profile.addStringProperty("mDesc");
        profile.addIntProperty("mThumbnail");
        profile.addIntProperty("mLogo");
    }

    private static void addGuru(Schema schema){
        Entity guru = schema.addEntity("Guru");
        guru.setHasKeepSections(true);
        guru.setTableName("Guru");
        guru.addStringProperty("id").primaryKey();
        guru.addStringProperty("nama");
        guru.addStringProperty("jurusan");
        guru.addStringProperty("moto");
    }


}
